from config import get_environment
from core import create_app, socketio

app = create_app(get_environment())
socketio.run(app, host='127.0.0.1', port=8080)
