import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    FLASK = dict(
        SECRET_KEY=os.environ.get('SECRET_KEY', 'very secret key'),
        GLOBALS=dict()
    )


class DevelopmentConfigs(Config):
    FLASK = dict(
        Config.FLASK,
        DEBUG=True
    )
    BACKEND = dict(
        url=''
    )


class ProductionConfig(Config):
    FLASK = dict(
        Config.FLASK,
        DEBUG=False
    )


config = {
    'development': DevelopmentConfigs,
    'production': ProductionConfig,

    'default': DevelopmentConfigs
}


def get_environment():
    return os.environ.get('ENVIRONMENT', 'default')


def get_configs(environment=None):
    return config[environment or get_environment()]
