from flask import current_app as app


def register_blueprints():
    from . import some
    app.register_blueprint(some.bp)
