from flask import Blueprint, jsonify

from core import socketio

bp = Blueprint('some', __name__, url_prefix='/some')


@socketio.on('some')
def emit_some():
    socketio.emit('some')


@bp.route('/', methods=['GET'])
def get_some():
    return jsonify(some='some')
