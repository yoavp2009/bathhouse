from flask_socketio import SocketIO

socketio = SocketIO(async_mode='threading')


def create_app(environment):
    from flask import Flask

    app = Flask(__name__, instance_relative_config=True)

    # setup app configuration
    app.config.update(_get_configs(environment).FLASK)

    # apply the blueprints to the app
    with app.app_context():
        from core.blueprints import register_blueprints
        register_blueprints()

    # add socketio support
    socketio.init_app(app)

    return app


def _get_configs(environment=None):
    import config
    return config.get_configs(environment)
